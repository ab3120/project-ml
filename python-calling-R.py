# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 13:48:00 2019

@author: papnejar
"""
import numpy as np

from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestNeighbors
import pandas as pd
import os as os
from scipy.spatial.distance import pdist

from rpy2 import robjects
from rpy2.robjects.packages import importr, STAP
import rpy2.robjects.numpy2ri
import rpy2.robjects.pandas2ri
from rpy2.robjects import conversion 


#rpy2.robjects.numpy2ri.activate()
#rpy2.robjects.pandas2ri.activate()

os.chdir('C:/Users/papnejar/Desktop/ml-jupyter')

df = pd.read_csv('dataCleanedPhase1.csv')
#df_R = conversion.converter.py2ri(df)

# The R 'print' function
rprint = robjects.globalenv.get("print")
base = importr('base')
datasets = importr('datasets')
cluster = importr('cluster')


def gower_distances(data):
    matrix_function = 'as_matrix_daisy <- function(dresult){return(as.matrix(dresult))}'
    custom_package = STAP(matrix_function, "as_matrix_daisy")
    m = cluster.daisy(data, metric = robjects.StrVector(['gower']))
    distances = np.mat(custom_package.as_matrix_daisy(m))
    return distances


distances = gower_distances(df)
print(distances[0,:])

# classifier = KNeighborsClassifier(metric= gower_distances)
#neigh = NearestNeighbors(n_neighbors=3, metric= gower_metrics)
#neigh.fit(np.asarray(mtcars))